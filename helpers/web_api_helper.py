import requests

class WebApiHelper:
    def __init__(self, base_url, sub_path_check=""):
        self.base_url = base_url
        self.service_down = False

        # if requests.head(self.base_url).status_code == 200:
        #     print(self.base_url + sub_path_check + " seems to be down!")
        #     self.service_down = True 

    def post(self, path, data, headers=""):
        api_url = self.base_url + path
        r = requests.post(api_url, data=data, headers=headers)
        return r
