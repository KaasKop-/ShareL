ShareL
======
This is a program/framework that allows users to upload or create plugins to upload to online services to share anything they want.

### Plugins
Plugins available by default are SFTP and Imgur for now.

### Notes to developers of plugins
ShareL requires some information from a plugin and you must define these in 3 variables. 
An example from the imgur plugin:
```python
class imgur:
    service_name = "imgur"                      # What is the service called
    help_text = "Upload the file to imgur."     # The help text shown to users when they pass --help
    nargs = 1                                   # How many arguments does your plugin accept 
    init_succeeded = False
    def __init__(self):
        self.init_succeeded = True
        pass

    def upload(self, file):
        # This method must be implemented and at the very least must accept 1 parameter for the file path
        # This methos must return a list
        return [True, "url_for_uploaded_file", "not sure.."]
        pass
```
All plugin filenames must match the class name, so imgur.py implements the imgur class.

Due to the nature of loading plugins dynamically, module requirements are different per plugin as well. If you need an additional module to use in your plugin use importlib to import them and catch import errors and display it to the user. (I might rework this in the future.)


